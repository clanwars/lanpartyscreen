import {PipelineEntryAbstract} from './pipeline-entry-abstract';
import {ModuleType} from './moduleType';
import {News} from './news';

export class PipelineEntryNews extends PipelineEntryAbstract {
  type = ModuleType.News;
  payload: News;
}
