import {PipelineEntryAbstract} from './pipeline-entry-abstract';
import {ModuleType} from './moduleType';
import {Picture} from './picture';

export class PipelineEntryPicture extends PipelineEntryAbstract {
  type = ModuleType.Pictures;
  payload: Picture;
}
