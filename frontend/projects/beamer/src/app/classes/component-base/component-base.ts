import {Visual} from './visual';

export class ComponentBase {
  public visual: Visual;

  private visualsMap = new Map<number, Visual>([
    [1, {videoUrl: 'https://www.hsf-clanwars.de/videos/bokk.mp4', accentColor: '#FF00DE'}],
    [2, {videoUrl: 'https://www.hsf-clanwars.de/videos/breath_ctrl.mp4', accentColor: '#00F9FF'}],
    [3, {videoUrl: 'https://www.hsf-clanwars.de/videos/brokchrd.mp4', accentColor: '#ff6700'}],
    [4, {videoUrl: 'https://www.hsf-clanwars.de/videos/built.mp4', accentColor: '#00F9FF'}],
    [5, {videoUrl: 'https://www.hsf-clanwars.de/videos/cleanroom.mp4', accentColor: '#ff073a'}],
    [6, {videoUrl: 'https://www.hsf-clanwars.de/videos/crysblast.mp4', accentColor: '#FF00DE'}],
    [7, {videoUrl: 'https://www.hsf-clanwars.de/videos/crystmounts.mp4', accentColor: '#00F9FF'}],
    [8, {videoUrl: 'https://www.hsf-clanwars.de/videos/darknet.mp4', accentColor: '#ff073a'}],
    [9, {videoUrl: 'https://www.hsf-clanwars.de/videos/diamondrop.mp4', accentColor: '#37ff6a'}],
    [10, {videoUrl: 'https://www.hsf-clanwars.de/videos/eightytown.mp4', accentColor: '#FF00DE'}],
    [11, {videoUrl: 'https://www.hsf-clanwars.de/videos/glassladder.mp4', accentColor: '#FF00DE'}],
    [12, {videoUrl: 'https://www.hsf-clanwars.de/videos/milkcave.mp4', accentColor: '#FF00DE'}],
    [13, {videoUrl: 'https://www.hsf-clanwars.de/videos/okkk.mp4', accentColor: '#FF00DE'}],
    [14, {videoUrl: 'https://www.hsf-clanwars.de/videos/pinkvinyl.mp4', accentColor: '#FF00DE'}],
    [15, {videoUrl: 'https://www.hsf-clanwars.de/videos/rebalance.mp4', accentColor: '#FF00DE'}],
    [16, {videoUrl: 'https://www.hsf-clanwars.de/videos/strt.mp4', accentColor: '#FF00DE'}],
    [17, {videoUrl: 'https://www.hsf-clanwars.de/videos/glitchyghost.mp4', accentColor: '#37ff6a'}],
  ]);

  constructor() {
    this.visual = this.getRandomVisual();
    console.log('applying Visual: ' + JSON.stringify(this.visual));
  }

  private getRandomVisual(): Visual {
    const min = 1;
    const max = this.visualsMap.size;
    const index = Math.floor(Math.random() * (max - min + 1)) + min;
    return this.visualsMap.get(index);
  }
}

