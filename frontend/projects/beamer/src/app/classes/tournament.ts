import {Match} from './match';

export class Tournament {
  id: number;
  mode: string;
  name: string;
  playersPerTeam: number;
  teams: number;
  teamLimit: number;
  state: string;
  startTime: Date;
  matches: Match[];
}
