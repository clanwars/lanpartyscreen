import {PipelineEntryAbstract} from './pipeline-entry-abstract';
import {ModuleType} from './moduleType';
import {WelcomeMessage} from './welcome-message';

export class PipelineEntryWelcomeMessage extends PipelineEntryAbstract {
  type = ModuleType.WelcomeMessage;
  payload: WelcomeMessage;
}
