export class News {
  id: number;
  title: string;
  author: string;
  body: string;
  publishedAt: Date;
}
