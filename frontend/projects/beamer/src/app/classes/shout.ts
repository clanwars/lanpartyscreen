export class Shout {
  name: string;
  text: string;
  id: number;
  postedAt: Date;
}
