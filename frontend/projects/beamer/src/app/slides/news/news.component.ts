import {animate, style, transition, trigger} from '@angular/animations';
import {Component, Input, OnInit} from '@angular/core';
import {ComponentBase} from '../../classes/component-base/component-base';
import {PipelineEntryNews} from '../../classes/pipeline-entry-news';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss'],
  animations: [
    trigger('container', [
      transition(':enter', [
        style({bottom: '-200%', minHeight: '100vh'}),
        animate('2000ms ease-out'), style({bottom: '0%'})
      ]),
    ])
  ]
})
export class NewsComponent extends ComponentBase implements OnInit {

  @Input()
  message: PipelineEntryNews;

  public date: Date;

  constructor() {
    super();
  }

  ngOnInit() {
    this.date = new Date(this.message.payload.publishedAt);
  }

}
