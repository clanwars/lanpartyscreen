import {animate, style, transition, trigger} from '@angular/animations';
import {Component, Input, OnInit} from '@angular/core';
import {ComponentBase} from '../../classes/component-base/component-base';
import {PipelineEntryShout} from '../../classes/pipeline-entry-shout';

@Component({
  selector: 'app-shouts',
  templateUrl: './shouts.component.html',
  styleUrls: ['./shouts.component.scss'],
  animations: [
    trigger('container', [
      transition(':enter', [
        style({bottom: '-200%', minHeight: '100vh'}),
        animate('1500ms ease-out'), style({bottom: '0%'})
      ]),
    ])
  ]
})

export class ShoutsComponent extends ComponentBase implements OnInit {

  @Input()
  message: PipelineEntryShout;

  constructor() {
    super();
  }

  ngOnInit() {
    setTimeout(() => this.message.isNew = false, 4000);
  }

}
