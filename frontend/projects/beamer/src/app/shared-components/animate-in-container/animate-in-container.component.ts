import {animate, state, style, transition, trigger} from '@angular/animations';
import {Component, Input, OnChanges, SimpleChanges} from '@angular/core';

@Component({
  selector: 'app-animate-in-container',
  templateUrl: './animate-in-container.component.html',
  styleUrls: ['./animate-in-container.component.scss'],
  animations: [
    trigger('container', [
      state('true', style({top: '0%', left: '0%'})),
      state('false', style({top: '0%', left: '800%'})),
      transition('void => true', [
        style({top: '800%', left: '0%'}),
        animate('800ms ease-out')
      ]),
      transition('true => false', [
        animate('400ms ease-in', style({top: '0%', left: '300%'}))
      ]),
    ]),
    trigger('wrapper', [
      state('true', style({height: '0rem', position: 'relative'})),
      state('false', style({height: '6rem', position: 'relative'})),
      transition('false => true', [
        animate('300ms ease-in-out', style({height: '0rem', position: 'relative'}))
      ]),
    ])
  ]
})
export class AnimateInContainerComponent implements OnChanges {

  @Input() show = true;

  public collapse = false;

  constructor() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.show.currentValue === false) {
      setTimeout(() => {
        this.collapse = true;
      }, 100);
    }
  }
}
