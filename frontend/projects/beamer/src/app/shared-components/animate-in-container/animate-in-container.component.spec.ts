import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AnimateInContainerComponent } from './animate-in-container.component';

describe('AnimateInContainerComponent', () => {
  let component: AnimateInContainerComponent;
  let fixture: ComponentFixture<AnimateInContainerComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AnimateInContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnimateInContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
