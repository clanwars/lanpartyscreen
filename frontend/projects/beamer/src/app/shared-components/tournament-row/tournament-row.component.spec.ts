import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TournamentRowComponent } from './tournament-row.component';

describe('TournamentRowComponent', () => {
  let component: TournamentRowComponent;
  let fixture: ComponentFixture<TournamentRowComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ TournamentRowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TournamentRowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
