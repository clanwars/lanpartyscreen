import { TestBed, inject } from '@angular/core/testing';

import { StaticPicturesService } from './static-pictures.service';

describe('StaticPicturesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [StaticPicturesService]
    });
  });

  it('should be created', inject([StaticPicturesService], (service: StaticPicturesService) => {
    expect(service).toBeTruthy();
  }));
});
