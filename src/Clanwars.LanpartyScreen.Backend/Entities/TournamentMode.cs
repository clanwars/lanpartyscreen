namespace Clanwars.LanpartyScreen.Backend.Entities;

public enum TournamentMode
{
    SingleElimination = 1,
    DoubleElimination = 2,
    Groups = 3,
    Points = 4,
    Randomize = 5,
    KingOfTheHill = 6,
    DeathMatch = 7
}