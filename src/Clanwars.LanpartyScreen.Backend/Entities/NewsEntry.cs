using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clanwars.LanpartyScreen.Backend.Entities;

[Table("News")]
public class NewsEntry
{
    [Key]
    public uint Id { get; set; }

    [Required]
    [StringLength(100, MinimumLength = 1)]
    public string Title { get; set; } = null!;

    [Required]
    [StringLength(100, MinimumLength = 1)]
    public string Author { get; set; } = null!;

    [Required]
    public string Body { get; set; } = null!;

    [Required]
    public DateTime PublishedAt { get; set; }
}