using System.Threading.Tasks;
using Clanwars.LanpartyScreen.Backend.Services.Pipeline;
using Microsoft.AspNetCore.SignalR;

namespace Clanwars.LanpartyScreen.Backend.Hubs;

public class PipelineHub : Hub
{
    private IPipelinePublisher _publisher;

    public PipelineHub(IPipelinePublisher publisher)
    {
        _publisher = publisher;
    }

    public override async Task OnConnectedAsync()
    {
        await Clients.Caller.SendAsync("autoplay", _publisher.PublisherAutoMode ? "on" : "off");
        await Clients.Caller.SendAsync("publish", _publisher.CurrentEntry);
        await base.OnConnectedAsync();
    }
}