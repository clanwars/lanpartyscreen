using System.Collections.Generic;
using System.Threading.Tasks;
using Clanwars.LanpartyScreen.Backend.Entities;

namespace Clanwars.LanpartyScreen.Backend.Repositories;

public interface IPicturesRepository : IRandomlyAccessible<PictureEntry>
{
    Task<IEnumerable<PictureEntry>> AllAsync();
    Task<PictureEntry?> FindAsync(uint itemId);
    Task<IEnumerable<PictureEntry>> LastAsync(uint limitLatest);
    Task<uint> InsertAsync(PictureEntry entry);
    Task UpdateAsync(PictureEntry entry);
    Task DeleteAsync(uint id);
}