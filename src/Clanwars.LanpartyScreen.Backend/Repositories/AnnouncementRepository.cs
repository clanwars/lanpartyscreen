using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Clanwars.LanpartyScreen.Backend.Contexts;
using Clanwars.LanpartyScreen.Backend.Entities;
using Clanwars.LanpartyScreen.Backend.Helpers.Exceptions;
using Microsoft.EntityFrameworkCore;

namespace Clanwars.LanpartyScreen.Backend.Repositories;

public class AnnouncementRepository : IAnnouncementRepository
{
    private readonly LanpartyScreenContext _context;

    public AnnouncementRepository(LanpartyScreenContext context)
    {
        _context = context;
    }

    public async Task<bool> DoesItemExistAsync(uint id)
    {
        return await _context.Announcements.CountAsync(ae => ae.Id == id) > 0;
    }

    public async Task<uint> InsertAsync(AnnouncementEntry entry)
    {
        await _context.Announcements.AddAsync(entry);
        await _context.SaveChangesAsync();
        return entry.Id;
    }

    public async Task UpdateAsync(AnnouncementEntry entry)
    {
        await _context.SaveChangesAsync();
    }

    public async Task DeleteAsync(uint id)
    {
        _context.Announcements.Remove(new AnnouncementEntry {Id = id});
        await _context.SaveChangesAsync();
    }

    public async Task<AnnouncementEntry?> FindAsync(uint id)
    {
        return await _context.Announcements.FirstOrDefaultAsync(a => a.Id == id);
    }

    public async Task<AnnouncementEntry> LatestAsync()
    {
        var result = await _context.Announcements
            .OrderByDescending(ae => ae.PostedAt)
            .FirstOrDefaultAsync();
        if (result == null)
        {
            throw new NoEntryInModuleException();
        }

        return result;
    }

    public async Task<IEnumerable<AnnouncementEntry>> AllAsync()
    {
        return await _context.Announcements.ToListAsync();
    }

    public async Task<IEnumerable<AnnouncementEntry>> LastAsync(int limitLatest)
    {
        return await _context.Announcements.OrderByDescending(ae => ae.Id).Take(limitLatest).ToListAsync();
    }
}
