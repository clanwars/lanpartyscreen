using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Clanwars.LanpartyScreen.Backend.Contexts;
using Clanwars.LanpartyScreen.Backend.Entities;
using Clanwars.LanpartyScreen.Backend.Helpers.Exceptions;
using Microsoft.EntityFrameworkCore;

namespace Clanwars.LanpartyScreen.Backend.Repositories;

public class NewsRepository : INewsRepository
{
    private readonly LanpartyScreenContext _context;

    public NewsRepository(LanpartyScreenContext context)
    {
        _context = context;
    }

    public async Task<bool> DoesItemExistAsync(uint id)
    {
        return await _context.News.CountAsync(n => n.Id == id) > 0;
    }

    public async Task<NewsEntry?> FindAsync(uint id)
    {
        return await _context.News.FirstOrDefaultAsync(n => n.Id == id);
    }

    public async Task<IEnumerable<NewsEntry>> AllAsync()
    {
        return await _context.News.ToListAsync();
    }

    public async Task<IEnumerable<NewsEntry>> LastAsync(uint limitLatest)
    {
        return await _context.News.OrderByDescending(n => n.PublishedAt).Take((int) limitLatest).ToListAsync();
    }

    public async Task<uint> InsertAsync(NewsEntry entry)
    {
        await _context.News.AddAsync(entry);
        await _context.SaveChangesAsync();

        return entry.Id;
    }

    public async Task UpdateAsync(NewsEntry entry)
    {
        await _context.SaveChangesAsync();
    }

    public async Task DeleteAsync(uint id)
    {
        _context.News.Remove(new NewsEntry {Id = id});
        await _context.SaveChangesAsync();
    }

    public async Task<NewsEntry> GetRandomAsync()
    {
        var allEntries = await AllAsync();
        var newsEntries = allEntries.ToList();
        if (!newsEntries.Any())
        {
            throw new NoEntryInModuleException();
        }

        var rnd = new Random();
        var entry = newsEntries[rnd.Next(newsEntries.Count)];

        return entry;
    }
}
