using System.Collections.Generic;
using System.Threading.Tasks;
using Clanwars.LanpartyScreen.Backend.Entities;

namespace Clanwars.LanpartyScreen.Backend.Repositories;

public interface IShoutRepository : IRandomlyAccessible<ShoutEntry>
{
    Task<IEnumerable<ShoutEntry>> AllAsync();
    Task<IEnumerable<ShoutEntry>> LastAsync(uint limitLatest);
    Task<bool> DoesItemExistAsync(uint id);
    Task<ShoutEntry?> FindAsync(uint id);
    Task InsertAsync(ShoutEntry shout);
    Task DeleteAsync(uint id);
}