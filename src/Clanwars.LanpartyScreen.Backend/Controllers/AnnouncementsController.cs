﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Clanwars.LanpartyScreen.Backend.Entities;
using Clanwars.LanpartyScreen.Backend.Messages;
using Clanwars.LanpartyScreen.Backend.Repositories;
using Clanwars.LanpartyScreen.Backend.Services.Pipeline;
using Mapster;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Clanwars.LanpartyScreen.Backend.Controllers;

[Route("api/v1/[controller]")]
[ApiController]
public class AnnouncementsController : ControllerBase
{
    private readonly IAnnouncementRepository _announcementRepository;
    private IPipelinePublisher _pipelinePublisher;

    public AnnouncementsController(IAnnouncementRepository announcementRepository,
        IPipelinePublisher pipelinePublisher)
    {
        _announcementRepository = announcementRepository;
        _pipelinePublisher = pipelinePublisher;
    }

    // GET api/v1/announcements
    [HttpGet]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    public async Task<ActionResult<IEnumerable<AnnouncementEntryResponse>>> Get([FromQuery] int? limit)
    {
        var result = limit is null
            ? await _announcementRepository.AllAsync()
            : await _announcementRepository.LastAsync(limit.Value);

        return Ok(result.Adapt<IEnumerable<AnnouncementEntryResponse>>());
    }

    // GET api/v1/announcements
    [HttpGet("{id}")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<IEnumerable<AnnouncementEntryResponse>>> GetItem([FromRoute] uint id)
    {
        var item = await _announcementRepository.FindAsync(id);

        if (item is null)
        {
            return NotFound();
        }

        return Ok(item.Adapt<AnnouncementEntryResponse>());
    }

    // POST api/v1/announcements
    [HttpPost]
    [ProducesResponseType(StatusCodes.Status201Created)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult<AnnouncementEntryResponse>> Post([FromBody] CreateAnnouncementEntry msg)
    {
        var newEntry = msg.Adapt<AnnouncementEntry>();

        await _announcementRepository.InsertAsync(newEntry);

        return CreatedAtAction("GetItem", new {announcementId = newEntry.Id}, newEntry.Adapt<AnnouncementEntryResponse>());
    }

    // PUT api/v1/announcements/5
    [HttpPut("{announcementId}")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<AnnouncementEntryResponse>> Put(
        [FromRoute] uint announcementId,
        [FromBody] UpdateAnnouncementEntry msg
    )
    {
        var oldEntry = await _announcementRepository.FindAsync(announcementId);
        if (oldEntry == null)
        {
            return NotFound();
        }

        msg.Adapt(oldEntry);

        await _announcementRepository.UpdateAsync(oldEntry);

        return Ok(oldEntry.Adapt<AnnouncementEntryResponse>());
    }


    // DELETE api/v1/announcements/5
    [HttpDelete("{announcementId}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<IActionResult> Delete([FromRoute] uint announcementId)
    {
        if (!await _announcementRepository.DoesItemExistAsync(announcementId))
        {
            return NotFound();
        }

        await _announcementRepository.DeleteAsync(announcementId);

        return NoContent();
    }

    // POST api/v1/announcements/2/activate
    [HttpPost("{announcementId}/activate/{timeout}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult> ActivateStaticPicture(
        [FromRoute] uint announcementId,
        [FromRoute] int timeout = 600
    )
    {
        var item = await _announcementRepository.FindAsync(announcementId);

        if (item is null)
        {
            return NotFound();
        }

        _pipelinePublisher.PublishStaticContent(item, timeout);
        return NoContent();
    }
}
