using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Clanwars.LanpartyScreen.Backend.Entities;
using Clanwars.LanpartyScreen.Backend.Messages;
using Clanwars.LanpartyScreen.Backend.Repositories;
using Clanwars.LanpartyScreen.Backend.Services.Pipeline;
using Mapster;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Clanwars.LanpartyScreen.Backend.Controllers;

[Route("api/v1/[controller]")]
[ApiController]
public class PicturesController : ControllerBase
{
    private readonly IPicturesRepository _picturesRepository;
    private readonly IPipelinePublisher _pipelinePublisher;

    public PicturesController(IPicturesRepository picturesRepository,
        IPipelinePublisher pipelinePublisher)
    {
        _picturesRepository = picturesRepository;
        _pipelinePublisher = pipelinePublisher;
    }

    // GET api/v1/Pictures
    [HttpGet]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<ActionResult<IEnumerable<PictureEntryResponse>>> Get([FromQuery] uint? limit)
    {
        var result = limit is null
            ? await _picturesRepository.AllAsync()
            : await _picturesRepository.LastAsync(limit.Value);

        var translatedPictures = result.Select(pic =>
        {
            var p = new PictureEntryResponse
            (
                pic.Id,
                Url: Url.Action("GetPicture", new {pictureId = pic.Id}),
                Title: pic.Title,
                ShowOnStream: pic.ShowOnStream
            );
            return p;
        });

        return Ok(translatedPictures);
    }

    // GET api/v1/Pictures/3
    [HttpGet("{pictureId:int}")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [Produces("image/jpeg", "image/png")]
    public async Task<ActionResult<byte[]>> GetPicture([FromRoute] uint pictureId)
    {
        var picture = await _picturesRepository.FindAsync(pictureId);

        if (picture is null)
        {
            return NotFound();
        }

        var regexMatch = Regex.Match(picture.Image, @"data:image/(?<type>.+?);base64,(?<data>.+)");
        var contentType = "image/" + regexMatch.Groups["type"].Value;
        var pictureData = regexMatch.Groups["data"].Value;

        Response.ContentType = contentType;
        var binData = Convert.FromBase64String(pictureData);

        return File(binData, contentType);
    }

    // POST api/v1/Pictures
    [HttpPost]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult<PictureEntryResponse>> Post([FromBody] CreatePictureEntry createMsg)
    {
        var entry = createMsg.Adapt<PictureEntry>();
        await _picturesRepository.InsertAsync(entry);

        var picture = new PictureEntryResponse
        (
            entry.Id,
            Url: Url.Action("GetPicture", new {pictureId = entry.Id}),
            Title: entry.Title,
            ShowOnStream: true
        );

        return Ok(picture);
    }

    // PUT api/v1/Pictures/{id}/show-on-stream
    [HttpPut("{pictureId:int}/show-on-stream")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult> ShowOnBeamer([FromRoute] uint pictureId)
    {
        var p = await _picturesRepository.FindAsync(pictureId);
        if (p is null)
        {
            return NotFound();
        }

        p.ShowOnStream = true;

        await _picturesRepository.UpdateAsync(p);

        return NoContent();
    }

    // PUT api/v1/Pictures/{id}/hide-on-stream
    [HttpPut("{pictureId:int}/hide-on-stream")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult> HideOnBeamer([FromRoute] uint pictureId)
    {
        var p = await _picturesRepository.FindAsync(pictureId);
        if (p == null)
        {
            return NotFound();
        }

        p.ShowOnStream = false;

        await _picturesRepository.UpdateAsync(p);

        return NoContent();
    }

    // DELETE api/v1/Pictures/2
    [HttpDelete("{pictureId:int}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult> Delete([FromRoute] uint pictureId)
    {
        var p = await _picturesRepository.FindAsync(pictureId);
        if (p == null)
        {
            return NotFound();
        }

        await _picturesRepository.DeleteAsync(pictureId);

        return NoContent();
    }

    // POST api/v1/Pictures/2/activate
    [HttpPost("{pictureId:int}/activate/{timeout:int}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<PictureEntryResponse>> ActivateStaticPicture(
        [FromRoute] uint pictureId,
        [FromRoute] int timeout = 600)
    {
        var picture = await _picturesRepository.FindAsync(pictureId);
        if (picture == null)
        {
            return NotFound();
        }

        _pipelinePublisher.PublishStaticContent(picture, timeout);
        return NoContent();
    }
}
