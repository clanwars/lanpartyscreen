using System;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Clanwars.LanpartyScreen.Backend.Entities;
using Clanwars.LanpartyScreen.Backend.Messages;
using Clanwars.LanpartyScreen.Backend.Services.Pipeline;
using Mapster;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Clanwars.LanpartyScreen.Backend.Controllers;

[Route("api/v1/[controller]")]
[ApiController]
public class WelcomeMessageController : ControllerBase
{
    private readonly IPipelinePublisher _publisher;

    public WelcomeMessageController(IPipelinePublisher publisher)
    {
        _publisher = publisher;
    }

    // POST api/v1/welcomemessage
    [HttpPost]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> AddWelcomeMessage(
        [FromBody] [Required] CreateWelcomeMessageEntry newWelcomeMessage)
    {
        var welcomeMessage = newWelcomeMessage with {CheckInTime = DateTime.Now};
        await _publisher.AddPipelineEntryAsync(welcomeMessage.Adapt<WelcomeMessageEntry>());
        return Ok();
    }
}