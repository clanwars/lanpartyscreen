using System;

namespace Clanwars.LanpartyScreen.Backend.Helpers.Exceptions;

public class ModuleDisabledException : Exception
{
}