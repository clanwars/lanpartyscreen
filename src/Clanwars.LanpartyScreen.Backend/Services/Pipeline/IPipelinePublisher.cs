using System.Collections.Generic;
using System.Threading.Tasks;
using Clanwars.LanpartyScreen.Backend.Entities;
using Clanwars.LanpartyScreen.Backend.Messages;
using Clanwars.LanpartyScreen.Backend.Messages.Pipeline;

namespace Clanwars.LanpartyScreen.Backend.Services.Pipeline;

public interface IPipelinePublisher
{
    bool PublisherAutoMode { get; set; }
    void PublishStaticContent(PictureEntry picture, int timeout = 10 * 60);
    void PublishStaticContent(AnnouncementEntry announcement, int timeout = 10 * 60);
    PipelineEntry CurrentEntry { get; }
    Task Skip();
    PipelineEntry? NextEntry();
    Task<int> AddPipelineEntryAsync(NewsEntry entry);
    Task<int> AddPipelineEntryAsync(ShoutEntry entry);
    Task<int> AddPipelineEntryAsync(WelcomeMessageEntry entry);
    Task<int> AddPipelineEntryAsync(IList<TournamentEntry> tournaments);
    Task<int> AddPipelineEntryAsync(TournamentMatchResult matchResult);
}