using System;
using System.ComponentModel.DataAnnotations;

namespace Clanwars.LanpartyScreen.Backend.Messages;

public record NewsEntryResponse
(
    uint Id,
    [StringLength(100, MinimumLength = 1)] string Title,
    [StringLength(100, MinimumLength = 1)] string Author,
    string Body,
    DateTime PublishedAt
);