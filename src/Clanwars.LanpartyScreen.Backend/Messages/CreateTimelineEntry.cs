using System;
using System.ComponentModel.DataAnnotations;

namespace Clanwars.LanpartyScreen.Backend.Messages;

public record CreateTimelineEntry
(
    [Required] DateTime? StartTime,
    string? Duration,
    [Required] string Title,
    string? AdditionalInformation
);